var gridData = Array(10000).fill(0).map(function(i){
  return 0;
});
var executing = false;
var steps = 0;
var cells;
var fpsDiv;
var stepsDiv;
var posistions = [-101,-100,-99,-1,1,99,100,101];

function buildGrid(){
  var grid = document.getElementById('grid');
  cells = gridData.map(function(item,index){
    var cell = document.createElement('div');
    cell.className = 'cell';
    cell.setAttribute('id',index);
    cell.setAttribute('alive',0);
    cell.addEventListener('click',toggle);
    grid.appendChild(cell);
    return cell;
  });
  fpsDiv = document.getElementById('fps');
  stepsDiv = document.getElementById('steps');
  document.getElementById('stop').disabled = true;
  stepsDiv.innerHTML = steps;
}

function toggle(event){
  var elm = event.srcElement;
  var index = parseInt(elm.getAttribute('id'));
  var alive = parseInt(elm.getAttribute('age'));
  gridData[index] = alive?0:1;
  elm.setAttribute('age',alive?0:1);
}

function step(){
  var oldData = gridData.slice();
  for(var i = 0,len = cells.length;i < len;i++){
    var alive = checkLife(i,oldData);
    if(oldData[i]){
      if(alive){
        if(oldData[i] < 4){
          var age = oldData[i] + 1;
          cells[i].setAttribute('age', age);
          gridData[i] = age;
        }
      }else{
        cells[i].setAttribute('age',0);
        gridData[i] = 0;
      }
    }else{
      if(alive){
        cells[i].setAttribute('age',1);
        gridData[i] = 1;
      }
    }
  }
  stepsDiv.innerHTML = ++steps;
}

function checkLife(index,data){
  var neighbors = 0;
  for(var j = 0, lenj = posistions.length; j < lenj; j++){
    if(data[index + posistions[j]]){
      neighbors++;
      if(neighbors === 4){
        j = lenj;
      }
    }
  }
  switch(neighbors){
    case 2:
      return data[index];
    case 3:
      return 1;
    default:
      return 0;
  }
}

function run(){
  executing = true;
  document.getElementById('step').disabled = true;
  document.getElementById('run').disabled = true;
  document.getElementById('reset').disabled = true;
  document.getElementById('stop').disabled = false;
  var frames = 0;
  var totalFrames = 0;
  var timeStart = new Date().getTime();

  var fpsCounter = setInterval(function(){
    totalFrames += frames;
    fpsDiv.innerHTML = frames;
    var timeNow = new Date().getTime();
    document.getElementById('avgfps').innerHTML = (totalFrames/((timeNow - timeStart)/1000)).toFixed(2);
    frames = 0;
    if(!executing){
      clearInterval(fpsCounter);
    }
  },1000);
  function frame(){
    if(executing){
      frames++;
      step();
      setTimeout(function(){
        frame();
      },0);
    }
  }
  frame();
}

function stop(){
  executing = false;
  document.getElementById('step').disabled = false;
  document.getElementById('run').disabled = false;
  document.getElementById('stop').disabled = true;
  document.getElementById('reset').disabled = false;
}

function placeRandom(){
  cells.forEach(function(cell2,index){
    if(Math.random() < 0.2){
      cell2.setAttribute('age',1);
      gridData[index] = 1;
    }
  });
}

function reset(){
  gridData = Array(10000).fill(0).map(function(i){
    return 0;
  });
  cells.forEach(function(cell){
    cell.setAttribute('age',0);
  });
  steps = 0;
  stepsDiv.innerHTML = steps;
  fpsDiv.innerHTML = '';
}

function save(){
  console.log(gridData);
}
